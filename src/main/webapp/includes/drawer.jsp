<div id="drawerExample"
	class="drawer drawer-inverse dw-xs-10 dw-sm-4 dw-md-4 dw-lg-2 fold"
	aria-labelledby="drawerExample">
	<!-- <div class="drawer-controls">
		<a href="#drawerExample" data-toggle="drawer" href="#drawerExample"
			aria-foldedopen="false" aria-controls="drawerExample"
			class="btn btn-primary btn-sm">Menu</a>
	</div> -->
	<div class="drawer-contents">
		<div class="drawer-heading">
			<h2 class="drawer-title">
				Select User <a href="#drawerExample"
					style="float: right; color: white;" type="button" class="close"
					data-toggle="drawer" aria-foldedopen="false"
					aria-controls="drawerExample"><i class="fa fa-close"></i></a>
			</h2>
		</div>
		<div id="users" class="drawer-body">
			<div class="form-group">
				<input class="form-control" id="find_users" placeholder="user...">
			</div>
			<ul id="user_list" class="drawer-fullnav">
			</ul>
			<ul id="select_user" class="drawer-fullnav"></ul>
			<!-- Date picker -->
			<div class="input-daterange input-group" id="datepicker">
				<input type="text" class="form-control" name="start" /> 
				<span class="input-group-addon">to</span> 
				<input type="text" class="form-control" name="end" /> 
				<span class="input-group-btn">
					<button class="btn btn-default" type="button" id="reportUser">view</button>
				</span>
			</div>


		</div>
		<div class="drawer-footer locked ">
			<hr />
			<small>&copy; Stefanos Gatsios</small> <br /> <small>&copy;
				Eleutheria Plakida</small>
		</div>
	</div>
</div>