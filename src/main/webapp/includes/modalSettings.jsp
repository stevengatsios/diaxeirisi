<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<div class="modal fade" id="modalSettings" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content navbar-inverse">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Settings</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id='settingsForm'>
					<div class="form-group">
						<label for="inputQuantum" class="col-sm-5 control-label">Time
							Quantum</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputQuantum">
							<div class="input-group-addon">s</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputTimeCutoff" class="col-sm-5 control-label">Split
							Time</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputTimeCutoff">
							<div class="input-group-addon">s</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputTimeMax" class="col-sm-5 control-label">Time
							Max (Clustering)</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputTimeMax">
							<div class="input-group-addon">s</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputTimeMin" class="col-sm-5 control-label">Time
							Min (Clustering)</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputTimeMin">
							<div class="input-group-addon">s</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputDistanceMax" class="col-sm-5 control-label">Distance
							Max (Clustering)</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputDistanceMax">
							<div class="input-group-addon">m</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEps" class="col-sm-5 control-label">Epsilon
							(Clustering)</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputEps">
							<div class="input-group-addon">m</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputMinPts" class="col-sm-5 control-label">Minimum
							Points (Clustering)</label>
						<div class="col-sm-4 input-group">
							<input type="text" class="form-control" id="inputMinPts">
							<div class="input-group-addon">clusters</div>
						</div>
					</div>
					<div class="form-group">
						<label for="inputDateStart" class="col-sm-5 control-label">Date
							range (Clustering)</label>
						<div class="col-sm-4 input-group input-daterange" id="inputDate" style='width : 33.3333%;'>
							<input type="text" class="form-control" name="start"
								id="inputDateStart"></input> <span class="input-group-addon">to</span>
							<input type="text" class="form-control" name="end"
								id="inputDateEnd"></input>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id='saveChanges'>Save
					changes</button>
			</div>
		</div>
	</div>
</div>