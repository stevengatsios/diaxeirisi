<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:image" content="icons/logo.png" />
<meta property="og:title" content="Why The Fi" />
<meta property="og:description" content="Something like Mozilla Location Service, but worse!" />
<style>
.navbar-nav>.active>a {
	background-color: #494171;
	outline: 0px;
}

html {
	height: 100%;
}

body {
	padding-top: 50px;
	width: 100%;
	height: 100%;
}

.map {
	height: 100%;
	width: 100%;
}
</style>
<!-- Bootstrap -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<link href="css/bootstrap-datepicker3.standalone.min.css"
	rel="stylesheet">
<link href="css/bootstrap-drawer.css" rel="stylesheet">
<link rel="shortcut icon" href="icons/favicon.ico">


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
