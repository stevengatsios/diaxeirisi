<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.jsp">
				<img src="icons/logo.png" alt="">
			</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
			<li><a href="#drawerExample" id="show_users" data-toggle="drawer" href="#drawerExample" aria-foldedopen="false" aria-controls="drawerExample">Users <i class="fa fa-user fa-1x"></i></a></li>
				<li><a href="#wifi" id="all_wifi">WiFi <i class="fa fa-wifi fa-1x"></i></a></li>
				<li><a href="#bases" id="all_bases">Bases <i class="fa fa-signal fa-1x"></i></a></li>
				<li class=''><a href="#stay" id="stayPoints">Stay Points <i class="fa fa-eye fa-1x"></i></a></li>
				<li class='disabled'><a href="#battery" id="battery">Battery <i class="fa fa-mobile fa-1x"></i></a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#statistics" id='drop_marker'>Drop <i class="fa fa-wifi"></i></a></li>
			 	<li><a href="#statistics" id='statisticsBattery' data-toggle="modal" data-target="#modalStatistics">Battery <i class="fa fa-bar-chart"></i></a></li>
			 	<li><a href="#statistics" id='statisticsNetwork' data-toggle="modal" data-target="#modalStatistics">Network <i class="fa fa-bar-chart"></i></a></li>
			 	<li><a href="#settings" id='settingsModal' data-toggle="modal" data-target="#modalSettings">Settings <i class="fa fa-gear"></i></a></li>
			 </ul>
		</div>
	</div>
</nav>