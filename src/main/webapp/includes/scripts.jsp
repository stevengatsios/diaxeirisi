<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>	 -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/library/bootstrap.min.js"></script>
<script src="js/library/drawer.js"></script> 
<script src="js/library/underscore.js"></script>
<script src="js/library/backbone.js"></script>
<script src="js/library/typeahead.bundle.js"></script>
<script src="js/library/bootstrap-datepicker.min.js"></script>
<script src="js/library/amcharts/amcharts.js"></script>
<script src="js/library/amcharts/serial.js"></script>
<script src="js/library/amcharts/pie.js"></script>
<script src="js/library/amcharts/themes/custom.js"></script>
<script src="js/init.js"></script>
<script src="js/wifi.js"></script>
<script src="js/base.js"></script>
<script src="js/settings.js"></script>
<script src="js/autocomplete.js"></script>
<script src="js/dropWifi.js"></script>
<script src="js/stayPoints.js"></script>
<script src="js/batteryStatistics.js"></script>
<script src="js/networkStatistics.js"></script>