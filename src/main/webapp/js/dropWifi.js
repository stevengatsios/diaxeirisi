/**
 * 
 */


var dropMarker;


$(function(){
	 dropMarker = new google.maps.Marker({
		position : map.getCenter(),
		icon : {
			path : google.maps.SymbolPath.CIRCLE,
			strokeColor : 'gold',
			scale : 10
		},
		draggable : true
	});
	google.maps.event.addListener(dropMarker, 'dragend', dropMarkerDrag);
	dropMarker.model = Backbone.Model.extend({
		urlRoot : '/diaxeirisi-webapp/webapi/wifi/near'
		
	});
});


$('#drop_marker').on('click', function(event) {
	var label = $("#drop_marker");
	if (label.parent().hasClass('active')) {
		label.parent().removeClass('active');
		dropMarker.setMap(null);
		wifiView.markers.length = 0;
		wifiView.collection.reset();
		$("#battery_graph").hide();
	} else {
		label.parent().addClass("active");
		dropMarker.setPosition(map.getCenter());
		dropMarker.setMap(map);
	}
});

function dropMarkerDrag(event) {
	var wifi = new dropMarker.model();
	var position = dropMarker.getPosition();
	var response = wifi.fetch({
		data : $.param({
			latitude : position.lat(),
			longitude : position.lng()
		})
	});
	response.success(function(data){
		wifiView.markers.length = 0;
		wifiView.collection.reset(data.wifi);
		$("#battery_graph").show();
		
		var sorted = data.spectrum.sort(function (a,b) {
			return a.level < b.level;
		});
		var max2 = sorted[1].level; //second biggest
		$.each(data.spectrum,function (index,item){
			if(item.level >= max2) {
				item.lineColor = "#b6d2ff";
			} else {
				item.lineColor ="#a47b22";
			}
		});
		data.spectrum.sort(function (a,b) {
			return a.frequency > b.frequency;
		});
		var chart = AmCharts.makeChart("battery_graph", {
			"type" : "serial",
			"theme" : "custom",
			"dataProvider" : data.spectrum,
			"valueAxes" : [ {
				"gridColor" : "#FFFFFF",
				"gridAlpha" : 0.2,
				"dashLength" : 0,
				"maximum" : 100,
				"minimum" : 0,
				"title": "Normalized level"
			} ],
			"gridAboveGraphs" : true,
			"startDuration" : 1,
			"graphs" : [ {
				"balloonText" : "[[frequency]]: <b>[[level]]%</b>",
				"fillAlphas" : 0.8,
				"lineAlpha" : 0.2,
				"colorField" : "lineColor",
				"type" : "column",
				"valueField" : "level"
			} ],
			"chartCursor" : {
				"categoryBalloonEnabled" : false,
				"cursorAlpha" : 0,
				"zoomable" : false
			},
			"categoryField" : "frequency",
			"categoryAxis" : {
				"gridPosition" : "start",
				"title" : "Frequency MHz"
			},
			"export" : {
				"enabled" : true
			}
		});
	});
	
}