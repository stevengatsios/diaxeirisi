/**
 * 
 */
var BaseStation = Backbone.Model.extend({

	getInfo : function() {
		var contentString = '<div id="content">' + '<div id="siteNotice">'
				+ '</div>' + '<h2 id="firstHeading" class="firstHeading">'
				+ this.attributes.operator + '</h2>' + '<div id="bodyContent">'
				+ '<b>cid</b> ' + this.attributes.cid + '<br/>' + '<b>lac</b> '
				+ this.attributes.lac + '<br/><b>lat</b> '
				+ this.attributes.location.x + '<br/><b>lon</b> '
				+ this.attributes.location.y + '</div>' + '</div>';

		var infowindow = new google.maps.InfoWindow({
			content : contentString,
			maxWidth : 300
		});
		return infowindow;
	},

	getMarker : function() {
		var image = {
			url : 'icons/base_small.png',
			size : new google.maps.Size(32, 32),
			origin : new google.maps.Point(0, 0),
			anchor : new google.maps.Point(16, 16)
		};
		var model = this.attributes;
		var myLatLng = new google.maps.LatLng(model.location.x,
				model.location.y);
		var marker = new google.maps.Marker({
			position : myLatLng,
			map : map,
			icon : image,
			title : model.operator
		});
		return marker;
	}

});

var BaseStationCollection = Backbone.Collection.extend({
	model : BaseStation,
	url : "/diaxeirisi-webapp/webapi/base",
});

var BaseStationView = Backbone.View.extend({
	el : $('#navbar'),
	initialize : function() {
		console.log('BaseStationView init.')
		this.markers = [];
		this.label = $("#all_bases");
		this.collection = new BaseStationCollection();
		this.collection.bind("reset", _.bind(this.render, this));
	},

	events : {
		"click #all_bases" : 'clicked'
	},

	clicked : function(e) {
		e.preventDefault();
		if (this.collection.length == 0) {
			var bounds = map.getBounds();
			this.collection.fetch({
				data : $.param({
					SWLat : bounds.getSouthWest().A,
					SWLon : bounds.getSouthWest().F,
					NELat : bounds.getNorthEast().A,
					NELon : bounds.getNorthEast().F,
				}),
				reset : true
			});
		}
		if (this.label.parent().hasClass('active')) {
			this.label.parent().removeClass('active');
			BaseStationMarkers.clearMarkers();
		} else {
			this.label.parent().addClass("active");
			if (this.markers.length != 0) {
				this.render();
			}
		}
	},

	render : function() {
		var self = this;
		console.log('Render called.', this);
		console.log('with collection', this.collection);
		BaseStationMarkers.clearMarkers();
		if(this.collection.length != 0) {
			this.label.parent().addClass("active");
		} else {
			this.label.parent().removeClass('active');
		}
		if (this.markers.length == 0) {
			this.collection.each(function(item, i) {
				var marker = item.getMarker();
				self.markers.push(marker);
				google.maps.event.addListener(marker, 'click', function() {
					item.getInfo().open(map, marker);
				});
			});
		}
		BaseStationMarkers.addMarkers(this.markers);
	}
});

var baseStationView = new BaseStationView();
