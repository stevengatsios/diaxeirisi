/**
 * 
 */
var StayPointModel = Backbone.Model.extend({
	urlRoot : "/diaxeirisi-webapp/webapi/users/stay",
	parse : function(response) {
		return response;
	},
});

var StayLocationModel = Backbone.Model.extend({

});

var StayPointCollection = Backbone.Collection.extend({});

var StayPointView = Backbone.View.extend({
	el : $('#navbar'),
	initialize : function() {
		console.log('StayPointView init.')
		this.model = new StayPointModel();
		this.label =  $("#stayPoints");
		this.model.bind("sync", _.bind(this.render, this));
	},

	events : {
		"click #stayPoints" : 'clicked'
	},
	dmax : 10,
	tmax : 1000000,
	tmin : 100,
	eps : 10,
	minPts : 3,
	dateStart : 0,
	dateEnd : 9999999999999,

	clicked : function(e) {
		e.preventDefault();
		if (this.label.parent().hasClass('active')) {
			this.label.parent().removeClass('active');
			$.each(StayPoints, function(index, item) {
				item.setMap(null);
			});
		} else {
			this.label.find('i').removeClass('fa-eye');
			this.label.find('i').addClass('fa-spinner fa-pulse');
			this.label.parent().addClass("active");
			if (typeof UserSelected != 'undefined' && UserSelected != null) {
				this.model.set({
					id : UserSelected.attributes.id
				});
				this.model.fetch({
					data : $.param({
						dmax : this.dmax,
						tmax : this.tmax,
						tmin : this.tmin,
						from : this.dateStart,
						to : this.dateEnd
					})
				});
			} else {
				this.model.fetch({
					data : $.param({
						dmax : this.dmax,
						tmax : this.tmax,
						tmin : this.tmin,
						eps : this.eps,
						minPts : this.minPts,
						from : this.dateStart,
						to : this.dateEnd
					})
				});
			}
		}
	},

	render : function() {
		var self = this;
		var flag = false;
		this.label.find('i').removeClass('fa-spinner fa-pulse');
		this.label.find('i').addClass('fa-eye');
		var strokeColor = '#A42827';
		var bounds = new google.maps.LatLngBounds();
		$.each(StayPoints, function(index, item) {
			item.setMap(null);
		});
		StayPoints.length = 0;
		if (typeof UserSelected == 'undefined' || UserSelected == null) {
			$.each(this.model.attributes, function(index, item) {
				var pathLine = [];
				flag = true;
				for (var i = 0; i < item.length; i++) {
					pathLine.push(new google.maps.LatLng(item[i].location.x,
							item[i].location.y));
					bounds.extend(pathLine[i]);
				}
				var polygon = new google.maps.Polygon({
					paths : pathLine,
					strokeColor : strokeColor,
					strokeOpacity : 0.8,
					strokeWeight : 2,
					fillColor : strokeColor,
					fillOpacity : 0.15
				});
				var marker = new google.maps.Marker({
					position : pathLine[0],
					map : map,
					title : "Cluster"
				});
				
				polygon.setMap(map);
				StayPoints.push(polygon);
				StayPoints.push(marker);

			});
		} else {
			$.each(this.model.attributes, function(index, item) {
				if (typeof item.location == 'undefined') {
					return;
				}
				var LatLng = new google.maps.LatLng(item.location.x,
						item.location.y);
				var circle = new google.maps.Circle({
					strokeColor : strokeColor,
					strokeOpacity : 1.0,
					strokeWeight : 3,
					center : LatLng,
					radius : self.dmax
				});
				flag = true;
				bounds.extend(LatLng);
				strokeColor = changeHue(strokeColor, 20);
				circle.setMap(map);
				StayPoints.push(circle);
			});
		}
		if (flag) {
			map.fitBounds(bounds);
		}
		console.log('Render called.', this);
		console.log('with collection', this.model.attributes);
	}

});

var stayPointView = new StayPointView();
