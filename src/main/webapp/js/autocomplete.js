var usersLookahead = new Bloodhound({
	datumTokenizer : Bloodhound.tokenizers.obj.whitespace('user'),
	queryTokenizer : Bloodhound.tokenizers.whitespace,
	prefetch : '/diaxeirisi-webapp/webapi/users/like',
	remote : {
		url : '/diaxeirisi-webapp/webapi/users/like?q=%QUERY',
		wildcard : '%QUERY'
	}
});

var UserSelected;
var Gps = Backbone.Model.extend({});
var Battery = Backbone.Model.extend({});

var User = Backbone.Model.extend({
	urlRoot : '/diaxeirisi-webapp/webapi/users',
	getName : function() {
		var model = this.attributes;
		return model.user;
	},
	getContribution : function() {
		var model = this.attributes;
		return model.contribution;
	}

});

var UserReport = Backbone.Model.extend({
	urlRoot : '/diaxeirisi-webapp/webapi/users/report',
	initialize : function() {
		// assuming Players a collection of players
		this.set('bases', new BaseStationCollection);
		this.set('wifi', new WifiCollection);
	},
	model : {
		gps : Gps,
		battery : Battery
	},
	parse : function(response) {
		return response;
	}
});

UserReport.quantum = 1;
UserReport.timeCutoff = 1000; //1 hour in ms

UserReport.batteryChartOptions = {
	type : "serial",
	theme : "custom",
	marginRight : 80,
	valueAxes : [ {
		position : "left",
		title : "Battery level",
		maximum : 100,
		minimum : 0
	} ],
	graphs : [ {
		id : "g1",
		fillAlphas : 0.4,
		valueField : "level",
		//fillColorsField : "lineColor",
		balloonText : "<div style='margin:5px; font-size:19px;'>Battery:<b>[[value]]%</b><br>Plugged : [[plugged]]<br>Temp : [[temperature]]</div>"
	} ],
	chartCursor : {
		categoryBalloonDateFormat : "JJ:NN, DD MMMM",
		cursorPosition : "mouse"
	},
	categoryField : "timestamp",
	categoryAxis : {
		minPeriod : "mm",
		parseDates : true
	},
	export : {
		enabled : true
	}
};


UserReport.render = function (data) {
	var pathLine = [];
	var currentPath;
	var self = UserReport;
	var LatLng;
	var flag = false;
	var curtime = 0;
	var date;
	var cutoff = self.timeCutoff * 1000; //convert ms to s
	var bounds = new google.maps.LatLngBounds();
	$.each(data.gps,function(index,item){
		LatLng = new google.maps.LatLng(item.location.x, item.location.y);
		//if time difference is more than timeCutoff start new polyline path
		date = new Date(item.date);
		if((date.getTime() - curtime) > cutoff) {
			pathLine.push([]);
			currentPath = pathLine[pathLine.length-1]; //getLast
		}
		curtime = date.getTime();
		bounds.extend(LatLng);
		currentPath.push(LatLng);
		flag = true;
	});
	//if existing polylines remove them
	if(Polylines.length != 0) {
		for(var i=0; i < Polylines.length ; i ++) {
			Polylines[i].setMap(null);
		}
		Polylines.length = 0;
	}
	//COMPUTE PATHS
	var strokeColor = '#A42827';
	for(var i = 0; i < pathLine.length; i++) {
		var tempLine;
		if (pathLine[i].length != 1) {
			//for paths
			tempLine = new google.maps.Polyline({
				path : pathLine[i],
				geodesic : true,
				strokeColor : strokeColor,
				strokeOpacity : 1.0,
				strokeWeight : 3
			});
			strokeColor = changeHue(strokeColor, 20);
			tempLine.setMap(map);
		} else if (pathLine[i].length == 1) {
			//for single points
			tempLine = new google.maps.Circle({
				strokeColor : strokeColor,
				strokeOpacity : 1.0,
				strokeWeight : 3,
				center :  pathLine[i][0],
				radius : 1
			});
			strokeColor = changeHue(strokeColor, 20);
			tempLine.setMap(map);
		}
		Polylines.push(tempLine);
	}
	//COMPUTE WIFI AND BASES
	wifiView.markers.length = 0;
	wifiView.collection.reset(data.wifi);
	baseStationView.markers.length = 0;
	baseStationView.collection.reset(data.bases);
	//ADD BATTERY GRAPH
	UserReport.batteryChartOptions.dataProvider = data.battery;
//	$.each(UserReport.batteryChartOptions.dataProvider,function(index,item){
//		if (item.plugged != 0) {
//			item.lineColor = "#b6d2ff";
//		} else {
//			item.lineColor ="#a47b22";
//		}
//	});
	var chart = AmCharts.makeChart('battery_graph',UserReport.batteryChartOptions);
	$("#battery_graph").show();
	$("#battery").parent().removeClass("disabled");
	$("#stayPoints").parent().removeClass("disabled");
	$("#battery").parent().addClass("active");
	if (flag) {
		map.fitBounds(bounds);
	}
	//hide panel
	$("#drawerExample").drawer("hide");
}

UserReport.report = function (event) {
	var from = Date.parse($('#datepicker [name="start"]').val());
	var to = Date.parse($('#datepicker [name="end"]').val());
	var params = { quantum : UserReport.quantum };
	if (!isNaN(from) && !isNaN(to)) {
		params.from = from;
		params.to = to;
	}
	var response = UserSelected.fetch({
		data : $.param(params)
	});
	response.success(UserReport.render);
}

User.deleteUser = function(event) {
	$("#select_user").empty();
	//$('#datepicker').datepicker("remove");
	$('#datepicker').hide();
	$("#find_users").show();
	$("#battery").parent().removeClass("active");
	$("#battery").parent().addClass("disabled");
	for(var i=0; i < Polylines.length ; i ++) {
		Polylines[i].setMap(null);
	}
	UserSelected = null;
	Polylines.length = 0;
	$("#battery_graph").hide();
	$("#users #user_list").show();
	wifiView.markers.length = 0;
	wifiView.collection.reset();
	baseStationView.markers.length = 0;
	baseStationView.collection.reset();
}

User.clickUser = function(data) {
	$("#find_users").hide();
	$("#users #user_list").hide(0,function() {
		$("#select_user").append('<li><a>' + data.user + '<span class="badge pull-right close-badge">' +
				'<i class="fa fa-close"></span></i></a></li>');
		$('.close-badge').on("click",User.deleteUser);
		$('#datepicker').show();
		var start = new Date(data.minDate - 86400000);
		var end = new Date(data.maxDate + 86400000);
		$("#datepicker [name='start']").datepicker('setStartDate',start);
		$("#datepicker [name='start']").datepicker('setEndDate',end);
		$("#datepicker [name='end']").datepicker('setStartDate',start);
		$("#datepicker [name='end']").datepicker('setEndDate',end);
		$("#reportUser").on("click",UserReport.report);
	});
}



usersLookahead.selectUser = function(e) {
	var username = $(this).clone() // clone the element
	.children() // select all the children
	.remove() // remove all the children
	.end() // again go back to selected element
	.text()
	var user = new User({id : username});
	UserSelected = new UserReport({id : username});
	user.fetch().success(User.clickUser);
}

usersLookahead.render = function(data) {
	var self = usersLookahead;
	var list = $("#users #user_list");
	var users = data;
	$.each(users, function(index, user) {
		if (list.children().length > self.sufficient) {
			return;
		}
		list.append('<li><a>' + user.user
				+ '<span class="badge pull-right contribution">'
				+ user.contribution + '</span></a></li>');
	});
	$('.contribution').parent().on('click', usersLookahead.selectUser);
}
usersLookahead.initialize();
// prepopulate user search
usersLookahead.search(this.value, usersLookahead.render, usersLookahead.render);

$('#find_users').keyup(
		function() {
			$("#users #user_list").empty();
			usersLookahead.search(this.value, usersLookahead.render,
					usersLookahead.render);
		});