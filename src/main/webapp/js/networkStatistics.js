/**
 * 
 */
var NetworkStat = Backbone.Model.extend({

});

var NetworkStatCollection = Backbone.Collection.extend({
	model : NetworkStat,
	url : "/diaxeirisi-webapp/webapi/statistics/network",
});

var NetworkStatView = Backbone.View.extend({
	el : $('#navbar'),
	initialize : function() {
		this.chart = null;
		console.log('NetworkStatView init.')
		this.collection = new NetworkStatCollection();
		this.collection.bind("reset", _.bind(this.render, this));
	},

	events : {
		"click #statisticsNetwork" : 'clicked'
	},

	clicked : function(e) {
		e.preventDefault();
		if (this.collection.length == 0) {
			this.collection.fetch({
				reset : true
			});
		}
		var label = $("#statisticsNetwork");
		if (label.parent().hasClass('active')) {
			label.parent().removeClass('active');
			this.chart.clear();
		} else {
			label.parent().addClass("active");
			if(this.chart != null) {
				this.chart.clear();
			}
			this.render();
		}
	},

	render : function() {
		var self = this;
		console.log('Render called.', this);
		console.log('with collection', this.collection);
		this.chart = AmCharts.makeChart("chartdiv", {"type": "pie",
			  "theme": "custom",
			  "dataProvider": this.collection.toJSON(),
			  "valueField": "users",
			  "titleField": "network",
			  "outlineAlpha": 0.4,
			  "balloonText": "[[users]] use [[network]] in [[country]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
			  "export": {
			    "enabled": true
			  }
		});
	}

});

var networkStatView = new NetworkStatView();
