var map;
var WifiMarkers;
var BaseStationMarkers;
var Polylines = [];
var StayPoints = [];

$(document).ready(
		function() {
			var styles;
			var mapOptions = {
				center : {
					lat : 37.9687188525696,
					lng : 23.72978971251035
				},
				zoom : 12,
				panControl : false,
				mapTypeControl : false,
				zoomControl : true,
				zoomControlOptions : {
					position : google.maps.ControlPosition.TOP_RIGHT
				},
				streetViewControl : false,
				scaleControl : false

			};
			$.ajax({
				dataType : "json",
				url : "styles/map_style.json",
				mimeType : "application/json",
				success : function(data) {
					styles = data;
					var styledMap = new google.maps.StyledMapType(styles, {
						name : "Styled Map"
					});
					map.mapTypes.set('map_style', styledMap);
					map.setMapTypeId('map_style');
				}
			});
			map = new google.maps.Map(document.getElementById('map-canvas'),
					mapOptions);				
			$.ajax({
				dataType : "json",
				url : "styles/wifi_style.json",
				mimeType : "application/json",
				success : function(data) {
					var wifiMarkerOptions = {
						gridSize : 50,
						maxZoom : 15,
						styles : data
					};
					WifiMarkers = new MarkerClusterer(map, [],
							wifiMarkerOptions);
				}
			});

			$.ajax({
				dataType : "json",
				url : "styles/bases_style.json",
				mimeType : "application/json",
				success : function(data) {
					var baseMarkerOptions = {
						gridSize : 25,
						maxZoom : 15,
						styles : data
					};
					BaseStationMarkers = new MarkerClusterer(map, [],
							baseMarkerOptions);
				}
			});
		});

$("#drawerExample").bind("hide.bs.drawer", function(e) {
	console.log("Drawer closed");
	$('#show_users').parent().removeClass("active");
});

$("#battery").on("click",function(event) {
	var label = $("#battery");
	var chart = $('#battery_graph');
	if (label.parent().hasClass('active')) {
		label.parent().removeClass('active');
		chart.hide();
	} else {
		label.parent().addClass("active");
		chart.show();
	}
})

$("#drawerExample").bind("show.bs.drawer", function(e) {
	console.log("Drawer open");
	$('#show_users').parent().addClass("active");
});
$('#datepicker').datepicker({
	format: "mm/dd/yyyy",
    todayBtn: "linked"
});

$('#inputDate').datepicker({
	format: "mm/dd/yyyy",
    todayBtn: "linked"
});

$("#datepicker").datepicker().on('show.bs.modal', function(event) {
    event.stopPropagation();
});

$("#datepicker").datepicker().on('hide.bs.modal', function(event) {
    event.stopPropagation();
});

$("#inputDate").datepicker().on('show.bs.modal', function(event) {
    event.stopPropagation();
});

$("#inputDate").datepicker().on('hide.bs.modal', function(event) {
    event.stopPropagation();
});
$('#datepicker').hide();
$("#battery_graph").hide();

//$('#datepicker').datepicker({
//	format: "dd/mm/yyyy",
//    todayBtn: "linked",
//    language: "el"
//});


$("#modalStatistics").bind("hide.bs.modal", function(e) {
	console.log("Modal closed");
	$('#statisticsBattery').parent().removeClass("active");
	$('#statisticsNetwork').parent().removeClass("active");
});

//COLOR FUNCTIONS
//Changes the RGB/HEX temporarily to a HSL-Value, modifies that value 
//and changes it back to RGB/HEX.

function changeHue(rgb, degree) {
	var hsl = rgbToHSL(rgb);
	hsl.h += degree;
	if (hsl.h > 360) {
		hsl.h -= 360;
	} else if (hsl.h < 0) {
		hsl.h += 360;
	}
	return hslToRGB(hsl);
}

//exepcts a string and returns an object
function rgbToHSL(rgb) {
	// strip the leading # if it's there
	rgb = rgb.replace(/^\s*#|\s*$/g, '');

	// convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
	if (rgb.length == 3) {
		rgb = rgb.replace(/(.)/g, '$1$1');
	}

	var r = parseInt(rgb.substr(0, 2), 16) / 255, g = parseInt(
			rgb.substr(2, 2), 16) / 255, b = parseInt(rgb.substr(4, 2), 16) / 255, cMax = Math
			.max(r, g, b), cMin = Math.min(r, g, b), delta = cMax - cMin, l = (cMax + cMin) / 2, h = 0, s = 0;

	if (delta == 0) {
		h = 0;
	} else if (cMax == r) {
		h = 60 * (((g - b) / delta) % 6);
	} else if (cMax == g) {
		h = 60 * (((b - r) / delta) + 2);
	} else {
		h = 60 * (((r - g) / delta) + 4);
	}

	if (delta == 0) {
		s = 0;
	} else {
		s = (delta / (1 - Math.abs(2 * l - 1)))
	}

	return {
		h : h,
		s : s,
		l : l
	}
}

//expects an object and returns a string
function hslToRGB(hsl) {
	var h = hsl.h, s = hsl.s, l = hsl.l, c = (1 - Math.abs(2 * l - 1)) * s, x = c
			* (1 - Math.abs((h / 60) % 2 - 1)), m = l - c / 2, r, g, b;

	if (h < 60) {
		r = c;
		g = x;
		b = 0;
	} else if (h < 120) {
		r = x;
		g = c;
		b = 0;
	} else if (h < 180) {
		r = 0;
		g = c;
		b = x;
	} else if (h < 240) {
		r = 0;
		g = x;
		b = c;
	} else if (h < 300) {
		r = x;
		g = 0;
		b = c;
	} else {
		r = c;
		g = 0;
		b = x;
	}

	r = normalize_rgb_value(r, m);
	g = normalize_rgb_value(g, m);
	b = normalize_rgb_value(b, m);

	return rgbToHex(r, g, b);
}

function normalize_rgb_value(color, m) {
	color = Math.floor((color + m) * 255);
	if (color < 0) {
		color = 0;
	}
	return color;
}

function rgbToHex(r, g, b) {
	return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}