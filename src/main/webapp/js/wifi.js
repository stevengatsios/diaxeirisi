/**
 * 
 */
var Wifi = Backbone.Model.extend({

	url : function() {
		return this.id ? '/diaxeirisi-webapp/webapi/wifi/'
				+ this.attributes.ssid : 'webapi/wifi';
	},

	getInfo : function() {
		var level = this.attributes.level;
		var contentString = '<div id="content">' + '<div id="siteNotice">'
				+ '</div>' + '<h2 id="firstHeading" class="firstHeading">'
				+ this.attributes.ssid + '</h2>' + '<div id="bodyContent">'
				+ '<b>MAC</b> ' + this.attributes.bssid + '<br/><b>MHz</b> '
				+ this.attributes.frequency + '<br/><b>lat</b> '
				+ this.attributes.location.x + '<br/><b>lon</b> '
				+ this.attributes.location.y + "<br/><b>level</b> "
				+ ((typeof level != 'undefined') ? level : 'NaN') + '</div>'
				+ '</div>';

		var infowindow = new google.maps.InfoWindow({
			content : contentString,
			maxWidth : 300
		});
		return infowindow;
	},

	getMarker : function() {
		var image = {
			url : 'icons/wifi_small.png',
			size : new google.maps.Size(32, 32),
			origin : new google.maps.Point(0, 0),
			anchor : new google.maps.Point(16, 16)
		};
		var model = this.attributes;
		var myLatLng = new google.maps.LatLng(model.location.x,
				model.location.y);
		var marker = new google.maps.Marker({
			position : myLatLng,
			map : map,
			icon : image,
			title : model.ssid
		});
		return marker;
	}

});

var WifiCollection = Backbone.Collection.extend({
	model : Wifi,
	url : "/diaxeirisi-webapp/webapi/wifi",
});

var WifiView = Backbone.View.extend({
	el : $('#navbar'),
	initialize : function() {
		console.log('WifiView init.')
		this.markers = [];
		this.label = $("#all_wifi");
		this.collection = new WifiCollection();
		this.collection.bind("reset", _.bind(this.render, this));
	},

	events : {
		"click #all_wifi" : 'clicked'
	},

	clicked : function(e) {
		e.preventDefault();
		if (this.markers.length == 0) {
			var bounds = map.getBounds();
			this.collection.fetch({
				data : $.param({
					SWLat : bounds.getSouthWest().A,
					SWLon : bounds.getSouthWest().F,
					NELat : bounds.getNorthEast().A,
					NELon : bounds.getNorthEast().F,
				}),
				reset : true
			});
		}
		if (this.label.parent().hasClass('active')) {
			this.label.parent().removeClass('active');
			WifiMarkers.clearMarkers();
		} else {
			this.label.parent().addClass("active");
			if (this.markers.length != 0) {
				this.render();
			}
		}
	},

	render : function() {
		var self = this;
		console.log('Render called.', this);
		console.log('with collection', this.collection);
		WifiMarkers.clearMarkers();
		if(this.collection.length != 0) {
			this.label.parent().addClass("active");
		} else {
			this.label.parent().removeClass('active');
		}
		if (this.markers.length == 0) {
			this.collection.each(function(item, i) {
				var marker = item.getMarker();
				self.markers.push(marker);
				google.maps.event.addListener(marker, 'click', function() {
					item.getInfo().open(map, marker);
				});
			});
		}
		WifiMarkers.addMarkers(this.markers);
	}
});

var wifiView = new WifiView();
