/**
 * 
 */

function restoreFromLocal() {
	$('#inputQuantum').val(localStorage.getItem('quantum'));
	$('#inputTimeCutoff').val(localStorage.getItem('timeCutoff'));
	$('#inputTimeMin').val(localStorage.getItem('tmin'));
	$('#inputTimeMax').val(localStorage.getItem('tmax'));
	$('#inputDistanceMax').val(localStorage.getItem('dmax'));
	$('#inputEps').val(localStorage.getItem('eps'));
	$('#inputMinPts').val(localStorage.getItem('minPts'));
	$('#inputDateStart').datepicker('setDate', new Date(Number.parseInt(localStorage.getItem('dateStart'))));
	$('#inputDateEnd').datepicker('setDate', new Date(Number.parseInt(localStorage.getItem('dateEnd'))));
}

function storeToLocal() {
	var quantum =  Number.parseInt($('#inputQuantum').val());
	var timeCutoff =  Number.parseInt($('#inputTimeCutoff').val());
	var tmin =  Number.parseInt($('#inputTimeMin').val());
	var tmax =  Number.parseInt($('#inputTimeMax').val());
	var dmax =  Number.parseFloat($('#inputDistanceMax').val());
	var eps =  Number.parseFloat($('#inputEps').val());
	var minPts =  Number.parseInt($('#inputMinPts').val());
	var dateStart =  new Date($('#inputDateStart').val()).getTime();
	var dateEnd =  new Date($('#inputDateEnd').val()).getTime();
	UserReport.quantum = quantum;
	UserReport.timeCutoff = timeCutoff;
	stayPointView.tmax = tmax;
	stayPointView.tmin = tmin;
	stayPointView.dmax = dmax;
	stayPointView.eps = eps;
	stayPointView.minPts = minPts;
	stayPointView.dateStart = dateStart;
	stayPointView.dateEnd = dateEnd;
	localStorage.setItem('quantum',quantum);
	localStorage.setItem('timeCutoff',timeCutoff);
	localStorage.setItem('tmax',tmax);
	localStorage.setItem('tmin',tmin);
	localStorage.setItem('dmax',dmax);
	localStorage.setItem('eps',eps);
	localStorage.setItem('minPts',minPts);
	localStorage.setItem('dateStart',dateStart);
	localStorage.setItem('dateEnd',dateEnd);
}

$(function(){
	if(localStorage.getItem('quantum') == null) {
		localStorage.setItem('quantum',UserReport.quantum);
	} else {
		UserReport.quantum = Number.parseInt(localStorage.getItem('quantum'));
	}
	if(localStorage.getItem('timeCutoff') == null) {
		localStorage.setItem('timeCutoff',UserReport.timeCutoff);
	} else {
		UserReport.timeCutoff = Number.parseInt(localStorage.getItem('timeCutoff'));
	}
	if(localStorage.getItem('tmax') == null) {
		localStorage.setItem('tmax',stayPointView.tmax);
	} else {
		stayPointView.tmax = Number.parseInt(localStorage.getItem('tmax'));
	}
	if(localStorage.getItem('tmin') == null) {
		localStorage.setItem('tmin',stayPointView.tmin);
	} else {
		stayPointView.tmin = Number.parseInt(localStorage.getItem('tmin'));
	}
	if(localStorage.getItem('dmax') == null) {
		localStorage.setItem('dmax',stayPointView.dmax);
	} else {
		stayPointView.dmax = Number.parseFloat(localStorage.getItem('dmax'));
	}
	if(localStorage.getItem('eps') == null) {
		localStorage.setItem('eps',stayPointView.eps);
	} else {
		stayPointView.eps = Number.parseFloat(localStorage.getItem('eps'));
	}
	if(localStorage.getItem('minPts') == null) {
		localStorage.setItem('minPts',stayPointView.minPts);
	} else {
		stayPointView.minPts = Number.parseInt(localStorage.getItem('minPts'));
	}
	if(localStorage.getItem('dateStart') == null) {
		localStorage.setItem('dateStart',stayPointView.dateStart);
	} else {
		stayPointView.dateStart = new Date(Number.parseInt(localStorage.getItem('dateStart'))).getTime();
	}
	if(localStorage.getItem('dateEnd') == null) {
		localStorage.setItem('dateEnd',stayPointView.dateEnd);
	} else {
		stayPointView.dateEnd = new Date(Number.parseInt(localStorage.getItem('dateEnd'))).getTime();
	}
	restoreFromLocal();
});

$("#modalSettings").bind("hide.bs.modal", function(e) {
	console.log("Modal closed");
	$('#settingsModal').parent().removeClass("active");
});

$("#modalSettings").bind("show.bs.modal", function(e) {
	console.log("Modal open");
	$('#settingsModal').parent().addClass("active");
	restoreFromLocal();
});

$("#saveChanges").on("click", function(e) { 
	storeToLocal();
	$("#modalSettings").modal('hide');
});
