/**
 * 
 */
var BatteryStat = Backbone.Model.extend({

});

var BatteryStatCollection = Backbone.Collection.extend({
	model : BatteryStat,
	url : "/diaxeirisi-webapp/webapi/statistics/battery",
});

var BatteryStatView = Backbone.View.extend({
	el : $('#navbar'),
	initialize : function() {
		this.chart = null;
		console.log('BatteryStatView init.')
		this.collection = new BatteryStatCollection();
		this.collection.bind("reset", _.bind(this.render, this));
	},

	events : {
		"click #statisticsBattery" : 'clicked'
	},

	clicked : function(e) {
		e.preventDefault();
		if (this.collection.length == 0) {
			this.collection.fetch({
				reset : true
			});
		}
		var label = $("#statisticsBattery");
		if (label.parent().hasClass('active')) {
			label.parent().removeClass('active');
			this.chart.clear();
		} else {
			label.parent().addClass("active");
			if(this.chart != null) {
				this.chart.clear();
			}
			this.render();
		}
	},

	render : function() {
		var self = this;
		console.log('Render called.', this);
		console.log('with collection', this.collection);
		this.chart = AmCharts.makeChart("chartdiv", {
			"type" : "serial",
			"theme" : "custom",
			"dataProvider" : this.collection.toJSON(),
			"valueAxes" : [ {
				"gridColor" : "#FFFFFF",
				"gridAlpha" : 0.2,
				"dashLength" : 0,
				"title": "People with less battery than 15%"
			} ],
			"gridAboveGraphs" : true,
			"startDuration" : 1,
			"graphs" : [ {
				"balloonText" : "Average battery for [[category]]:00: <b>[[avg]]%</b>",
				"fillAlphas" : 0.8,
				"lineAlpha" : 0.2,
				"type" : "column",
				"valueField" : "count"
			} ],
			"chartCursor" : {
				"categoryBalloonEnabled" : false,
				"cursorAlpha" : 0,
				"zoomable" : false
			},
			"categoryField" : "hour",
			"categoryAxis" : {
				"gridPosition" : "start",
				"title" : "Hour of day"
			},
			"export" : {
				"enabled" : true
			}
		});
	}

});

var batteryStationView = new BatteryStatView();
