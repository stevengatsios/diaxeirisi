<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="includes/header.jsp"%>
<script type="text/javascript"
	src="https://maps.googleapis.com/maps/api/js?sensor=false">
	
</script>
<script type="text/javascript" src="js/library/markerclusterer.min.js">	
</script>

<title>Dashboard</title>
</head>
<body class="has-drawer">
	<%@include file="includes/modalStatistics.jsp"%>
	<%@include file="includes/modalSettings.jsp"%>
	<%@include file="includes/navbar.jsp"%>
	<%@include file="includes/drawer.jsp"%>
		<div id="map-canvas" class="map" style="width: auto;"></div>
<!-- 	<div data-url="map-page" data-role="page" id="map-page"> -->
<!-- 		<div role="main" class="ui-content" id="map-canvas"> -->
<!-- 		</div> -->
<!-- 	</div> -->
	<div id='battery_graph' class='graph-footer'></div>
	<%@include file="includes/scripts.jsp"%>
</body>
</html>