#ifdef STANDARD
//compile with gcc -shared -o geomidpoint.so geomidpoint.c -I/usr/include/mysql -lm -w -fPIC
//copy with sudo cp geomidpoint.so /usr/lib/mysql/plugin/geomidpoint.so
/* STANDARD is defined, don't use any mysql functions */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef __WIN__
typedef unsigned __int64 ulonglong; /* Microsofts 64 bit types */
typedef __int64 longlong;
#else
typedef unsigned long long ulonglong;
typedef long long longlong;
#endif /*__WIN__*/
#else
#include <my_global.h>
#include <my_sys.h>
#if defined(MYSQL_SERVER)
#include <m_string.h>       /* To get strmov() */
#else
/* when compiled as standalone */
#include <string.h>
#define strmov(a,b) stpcpy(a,b)
#define bzero(a,b) memset(a,0,b)
#endif
#endif
#include <mysql.h>
#include <ctype.h>

#ifdef _WIN32
/* inet_aton needs winsock library */
#pragma comment(lib, "ws2_32")
#endif

#ifdef HAVE_DLOPEN

#if !defined(HAVE_GETHOSTBYADDR_R) || !defined(HAVE_SOLARIS_STYLE_GETHOST)
static pthread_mutex_t LOCK_hostname;
#endif

#include <math.h>

my_bool geomidpoint_init(UDF_INIT* initid, UDF_ARGS* args, char* message);
void geomidpoint_deinit(UDF_INIT* initid);
void geomidpoint_reset(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error);
void geomidpoint_clear(UDF_INIT* initid, char* is_null, char *error);
void geomidpoint_add(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char *error);
char* geomidpoint(UDF_INIT *initid, UDF_ARGS *args, char *result, unsigned long *length, char *is_null, char *error);




struct geomidpoint_data {
	long double sum;
	long double weight;
	longlong count;
};

my_bool geomidpoint_init(UDF_INIT *initid, UDF_ARGS *args,
	char *message)
{
	struct  geomidpoint_data *data;


	// check the arguments format
	if (args->arg_count != 2)
	{
		strcpy(message, "geomidpoint(coordinate,level) requires two arguments");
		return 1;
	}
	if (args->arg_type[0] != DECIMAL_RESULT)
	{
		strcpy(message, "geomidpoint(coordinate,level) requires a decimal for first argument");
		return 1;
	}
	if (args->arg_type[1] != INT_RESULT)
	{
		strcpy(message, "geomidpoint(coordinate,level) requires an integer for second argument");
		return 1;
	}

	if (!(data = (struct geomidpoint_data *) malloc(sizeof(struct geomidpoint_data))))
	{
		strmov(message, "Couldn't allocate memory");
		return 1;
	}
	initid->maybe_null = 0;        /* The result may be null */
	initid->decimals = 14;
	initid->max_length = 18;
	data->weight = 0;
	data->sum = 0;                     // set it to a value
	data->count = 0;
	initid->ptr = (struct geomidpoint_data*) data;
	return 0;
}

void geomidpoint_clear(UDF_INIT *initid, char *is_null __attribute__((unused)), char *error)
{
	/* The clear function resets the sum to 0 for each new group
	Of course you have to allocate a longlong variable in the init
	function and assign it to the pointer as seen above */
	struct geomidpoint_data *data = (struct geomidpoint_data *) initid->ptr;
	data->sum = 0.0;
	data->weight = 0.0;
	data->count = 0;
}

void geomidpoint_add(UDF_INIT *initid, UDF_ARGS *args,
	char *is_null __attribute__((unused)),
	char *error)
{
	if (args->args[0] && args->args[1])
	{
		//long double value = strtod((char*)args->args[0],NULL); //coordinate
		char * dbl = (char*)args->args[0];
		long double value = strtod(dbl, NULL);
		value *= (M_PI / 180.0);
		longlong level = *((longlong*)args->args[1]); //level of rssi
		long double weight = pow(10, level / 10.0);
		struct geomidpoint_data *data = (struct geomidpoint_data*) initid->ptr;
		// For each row the current value is added to the sum
		data->sum += value * weight;
		data->weight += weight;
		data->count++;
	}

}

char* geomidpoint(UDF_INIT *initid, UDF_ARGS *args __attribute__((unused)),
	char *result, unsigned long *length,
	char *is_null, char *error __attribute__((unused)))
{
	// And in the end the sum is returned
	struct geomidpoint_data *data = (struct geomidpoint_data *) initid->ptr;
	if (data == NULL) {
		strmov(error, "Pointer is null");
		*is_null = 1;
		return NULL;
	}
	if (data->count == 0) {
		*is_null = 1;
		return NULL;
	}
	else {
		*is_null = 0;
		long double resultValue = data->sum / data->weight;
		resultValue *= (180.0 / M_PI);
		snprintf(result, 31, "%2.14Lf", resultValue);
		*length = (uint)strlen(result);
		return result;
	}

}

void geomidpoint_deinit(UDF_INIT *initid)
{
	free(initid->ptr);
}


void geomidpoint_reset(UDF_INIT* initid, UDF_ARGS* args, char* is_null, char* message)
{
	geomidpoint_clear(initid, is_null, message);
	geomidpoint_add(initid, args, is_null, message);
}

#endif /* HAVE_DLOPEN */