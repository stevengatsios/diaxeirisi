package gr.uoa.di.resources;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import gr.uoa.di.hibernate.HibernateUtil;
import gr.uoa.di.hibernate.entities.BaseStations;
import gr.uoa.di.hibernate.entities.Gps;
import gr.uoa.di.hibernate.entities.Users;
import gr.uoa.di.pojo.AccessPointPojo;
import gr.uoa.di.pojo.BaseStationPojo;
import gr.uoa.di.pojo.BatteryPojo;
import gr.uoa.di.pojo.Clusterer;
import gr.uoa.di.pojo.GrahamScan;
import gr.uoa.di.pojo.StampListPojo;
import gr.uoa.di.pojo.StampPojo;
import gr.uoa.di.pojo.StayPointsList;
import gr.uoa.di.pojo.StayPojo;
import gr.uoa.di.pojo.UserPojo;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.math3.ml.clustering.Cluster;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

@Path("users")
public class UsersResource {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{username}")
	public Response getUser(@PathParam("username") String username) {
		Session session = null;
		Users user = null;
		UserPojo result = null;
		if (username == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			session = HibernateUtil.getSession();
			user = (Users) session.get(Users.class, username);
			Query datesRange = session
					.createQuery("select min(timestamp),max(timestamp) from Gps gps where gps.users = :name");
			datesRange.setParameter("name", user);
			@SuppressWarnings("unchecked")
			List<Object> dates = datesRange.list();
			if (user != null) {
				Object[] date = (Object[]) dates.get(0);
				result = new UserPojo(user.getUser(), user.getLatitude(),
						user.getLongitude(), user.getProbability(),
						user.getContribution());
				result.setMinDate((Timestamp) date[0]);
				result.setMaxDate((Timestamp) date[1]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("like")
	public Response likeUser(
			@QueryParam("q") @DefaultValue("") String username,
			@QueryParam("limit") @DefaultValue("10") int maxResult) {
		List<UserPojo> result = new ArrayList<UserPojo>();
		Session session = null;
		if (maxResult < 0) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			session = HibernateUtil.getSession();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.like("user", username + "%"));
			criteria.add(Restrictions.isNotNull("contribution"));
			criteria.addOrder(Order.desc("contribution"));
			criteria.setMaxResults(maxResult);
			for (Object a : criteria.list()) {
				Users u = (Users) a;
				UserPojo up = new UserPojo(u.getUser(), u.getLatitude(),
						u.getLongitude(), u.getProbability(),
						u.getContribution());
				result.add(up);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("report/{username}")
	public Response userReport(@PathParam("username") String username,
			@QueryParam("quantum") @DefaultValue("60") int quantum,
			@QueryParam("from") @DefaultValue("0") long fromL,
			@QueryParam("to") @DefaultValue("9999999999999") long toL) {
		Session session = null;
		quantum *= 1000; // millisecond to seconds
		Date from = new Date(fromL);
		Date to = new Date(toL);
		Users user = null;
		UserPojo result = null;
		if (username == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			session = HibernateUtil.getSession();
			user = (Users) session.get(Users.class, username);
			if (user != null) {
				result = new UserPojo(user.getUser(), user.getLatitude(),
						user.getLongitude(), user.getProbability(),
						user.getContribution());
				// ################### FIND GPS ########################
				Criteria gps = session.createCriteria(Gps.class);
				gps.add(Restrictions.between("timestamp", from, to));
				gps.add(Restrictions.eq("users", user));
				gps.addOrder(Order.asc("timestamp"));
				StampListPojo stamp = new StampListPojo(new Date(quantum));
				result.setGps(stamp);
				for (Gps g : (List<Gps>) gps.list()) {
					StampPojo s = new StampPojo(g.getTimestamp(),
							g.getLatitude(), g.getLongitude());
					stamp.add(s);
				}
				// ################### /FIND GPS ########################
				// ################### FIND AP ########################
				Query accessPoints = session
						.createQuery("select ap.id.id as id, ap.ssid as ssid, ap.bssid as bssid, avg(ap.level) as level,"
								+ " ap.frequency as frequency, ap2.id.latitude as latitude, ap2.id.longitude as longitude"
								+ " from AccessPoints ap, Ap ap2 where ap.users = :user"
								+ " and ap.timestamp between :from AND :to and ap.bssid=ap2.id.bssid group by ap.bssid");
				accessPoints.setParameter("user", user);
				accessPoints.setParameter("from", from);
				accessPoints.setParameter("to", to);
				accessPoints.setResultTransformer(Transformers.aliasToBean(AccessPointPojo.class));
				result.setWifi(accessPoints.list());
				// ################### //FIND AP ########################
				// ################### FIND BASES ########################
				Criteria baseStations = session.createCriteria(BaseStations.class);
				baseStations.add(Restrictions.eq("users", user));
				baseStations.add(Restrictions.between("timestamp", from, to));
				baseStations.add(Restrictions.isNotNull("latitude"));
				List<BaseStationPojo> bases = new ArrayList<BaseStationPojo>();
				result.setBases(bases);
				for (BaseStations b : (List<BaseStations>)baseStations.list()) {
					BaseStationPojo bp = new BaseStationPojo(b.getCid(), b
							.getCountry().getCountry(), b.getCountry()
							.getNetwork(), b.getCid(), b.getLac(),
							b.getLatitude(), b.getLongitude());
					bp.setDate(b.getTimestamp());
					bases.add(bp);
				}
				// ################### //FIND BASES ########################
				// ################### FIND BATTERY ########################
				Query battery = session.createQuery("select timestamp as timestamp,level as level,plugged as plugged,voltage as voltage,temperature as temperature"
				+" from Battery where users = :user and timestamp between :from and :to order by timestamp asc");
				battery.setParameter("user", user);
				battery.setParameter("from", from);
				battery.setParameter("to", to);
				battery.setResultTransformer(Transformers
						.aliasToBean(BatteryPojo.class));
				List<BatteryPojo> batteries = battery.list();
				result.setBattery(batteries);
				// ################### //FIND BATTERY ########################
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("stay/{username}")
	public Response userReport(@PathParam("username") String username,
			@QueryParam("from") @DefaultValue("0") long fromL,
			@QueryParam("to") @DefaultValue("9999999999999") long toL,
			@QueryParam("tmin") @DefaultValue("60") double tmin,
			@QueryParam("tmax") @DefaultValue("3600") double tmax,
			@QueryParam("dmax") @DefaultValue("100") double dmax) {
		Session session = null;
		//convert ms to sec
		tmin *= 1000.0;
		tmax *= 1000.0;
		Users user = null;
		List<StayPojo> result = null;
		StayPointsList stayPoint = new StayPointsList(tmin, tmax, dmax);
		Date from = new Date(fromL);
		Date to = new Date(toL);
		try {
			session = HibernateUtil.getSession();
			user = (Users) session.get(Users.class, username);
			if (user != null) {
				Criteria gps = session.createCriteria(Gps.class);
				gps.add(Restrictions.between("timestamp", from, to));
				gps.add(Restrictions.eq("users", user));
				gps.addOrder(Order.asc("timestamp"));
				result = stayPoint.getStayPointsList(gps.list());
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}
	
	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("stay")
	public Response stayPointsClusterer(@PathParam("username") String username,
			@QueryParam("from") @DefaultValue("0") long fromL,
			@QueryParam("to") @DefaultValue("9999999999999") long toL,
			@QueryParam("tmin") @DefaultValue("60") double tmin,
			@QueryParam("tmax") @DefaultValue("3600") double tmax,
			@QueryParam("dmax") @DefaultValue("100") double dmax,
			@QueryParam("eps") @DefaultValue("100") double eps,
			@QueryParam("minPts") @DefaultValue("3") int minPts) {
		Session session = null;
		//convert ms to sec
		tmin *= 1000.0;
		tmax *= 1000.0;
		if(minPts < 3) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		GrahamScan scan = new GrahamScan();
		List<StayPojo> stayLocations = new ArrayList<StayPojo>();
		List<Cluster<StayPojo>> clusters = null;
		List<List<StayPojo>> result = new ArrayList<List<StayPojo>>();
		StayPointsList stayPoint = new StayPointsList(tmin, tmax, dmax);
		Clusterer clusterer = new Clusterer(eps, minPts);
		Date from = new Date(fromL);
		Date to = new Date(toL);
		try {
			session = HibernateUtil.getSession();
			Criteria users = session.createCriteria(Users.class);
			for(Users u : (List<Users>)users.list()) {
				Criteria gps = session.createCriteria(Gps.class);
				gps.add(Restrictions.between("timestamp", from, to));
				gps.add(Restrictions.eq("users", u));
				gps.addOrder(Order.asc("timestamp"));
				stayLocations.addAll(stayPoint.getStayPointsList(gps.list()));
			}
			clusters = clusterer.cluster(stayLocations);
			for(Cluster<StayPojo> c : clusters) {
				List<StayPojo> hull = scan.computeHull(c.getPoints());
				result.add(hull);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

}
