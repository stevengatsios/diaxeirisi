package gr.uoa.di.resources;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import gr.uoa.di.hibernate.HibernateUtil;
import gr.uoa.di.hibernate.entities.Country;
import gr.uoa.di.hibernate.entities.CountryId;
import gr.uoa.di.hibernate.entities.Users;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("myresource")
public class MyResource {

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to
	 * the client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getIt() {
		return "Got it!";
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("user/{username}")
	public Response getUserById(@PathParam("username") String username) {
		Session session = null;
		Users user = null;
		List result = new ArrayList<String>();
		System.out.println("Hello World");
		try {
			session = HibernateUtil.getSession();
			Criteria criteria = session.createCriteria(Users.class);
			criteria.add(Restrictions.eq("user", username));
			criteria.setProjection(Projections.projectionList()
                    .add(Projections.groupProperty("user"))
                    .add(Projections.count("user")));
			result = criteria.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("country")
	public Country getCountryById(@QueryParam("mcc") int mcc,
			@QueryParam("mnc") int mnc) {
		Session session = null;
		CountryId id = new CountryId(mcc, mnc);
		Country country = null;
		try {
			session = HibernateUtil.getSession();
			country = (Country) session.get(Country.class, id);
			System.out.println(country);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return country;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("json")
	public Response getUser() {
		List<Integer> x = new ArrayList<Integer>();
		x.add(10);
		x.add(20);
		List<String> z = new ArrayList<String>();
		z.add("hello");
		z.add("world");
		Set<List> ret = new HashSet<List>();
		ret.add(x);
		ret.add(z);
		return Response.ok(ret).build();
	}

}
