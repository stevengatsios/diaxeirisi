package gr.uoa.di.resources;

import java.util.List;

import gr.uoa.di.hibernate.HibernateUtil;
import gr.uoa.di.pojo.BatteryStatisticsPojo;
import gr.uoa.di.pojo.NetworkStatisticsPojo;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

@Path("statistics")
public class StatisticsResource {

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("battery")
	public Response getBatteryStatistics(
			@QueryParam("level") @DefaultValue("15") int level) {
		Session session = null;
		List<BatteryStatisticsPojo> result = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session
					.createQuery("select HOUR(timestamp) as hour, count(*) as count,AVG(level) as avg "
							+ "from Battery where level < :level GROUP BY HOUR(timestamp)");
			query.setParameter("level", level);
			query.setResultTransformer(Transformers
					.aliasToBean(BatteryStatisticsPojo.class));
			result = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("network")
	public Response getNetworkStatistics() {
		Session session = null;
		List<Object[]> result = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session
					.createQuery("select count(distinct base.users) as users,ctry.network as network, "
							+ "ctry.country as country ,ctry.iso as iso "
							+ "from BaseStations base join base.country ctry group by ctry.network");
			query.setResultTransformer(Transformers
					.aliasToBean(NetworkStatisticsPojo.class));
			result = query.list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

}
