package gr.uoa.di.resources;

import gr.uoa.di.hibernate.HibernateUtil;
import gr.uoa.di.hibernate.entities.BaseStations;
import gr.uoa.di.pojo.BaseStationPojo;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hibernate.Query;
import org.hibernate.Session;

@Path("base")
public class BaseStationResource {

	@SuppressWarnings("unchecked")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listBases() {
		Session session = null;
		List<BaseStationPojo> result = new ArrayList<BaseStationPojo>();
		List<BaseStations> data = null;
		try {
			session = HibernateUtil.getSession();
			Query criteria = session
					.createQuery("from BaseStations base where base.latitude is not null group by base.cid");
			data = criteria.list();
			for (BaseStations bs : data) {
				BaseStationPojo bsp = new BaseStationPojo(bs.getId().getId(),
						bs.getCountry().getCountry(), bs.getCountry()
								.getNetwork(), bs.getCid(), bs.getLac(),
						bs.getLatitude(), bs.getLongitude());
				result.add(bsp);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}
}
