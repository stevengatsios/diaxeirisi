package gr.uoa.di.resources;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gr.uoa.di.hibernate.HibernateUtil;
import gr.uoa.di.hibernate.entities.AccessPointsId;
import gr.uoa.di.hibernate.entities.Ap;
import gr.uoa.di.hibernate.entities.ApId;
import gr.uoa.di.pojo.AccessPointPojo;
import gr.uoa.di.pojo.Location;
import gr.uoa.di.pojo.WifiSpectrum;
import gr.uoa.di.pojo.WifiSpectrumPojo;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;

@Path("wifi")
public class AccessPointsResource {

	@SuppressWarnings("rawtypes")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{name}")
	public Response listWifiByName(@PathParam("name") String name) {
		Session session = null;
		List<AccessPointPojo> result = new ArrayList<AccessPointPojo>();
		if (name == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		try {
			session = HibernateUtil.getSession();
			Query query = session
					.createQuery("SELECT id,ssid,bssid,frequency,geomidpoint(latitude,level),geomidpoint(longitude,level) "
							+ "FROM AccessPoints WHERE ssid = :name GROUP BY bssid");
			query.setParameter("name", name);
			List temp = query.list();
			for (Object t : temp) {
				Object[] tt = (Object[]) t;
				AccessPointsId apid = (AccessPointsId) tt[0];
				String ssid = (String) tt[1];
				String bssid = (String) tt[2];
				Integer frequency = (Integer) tt[3];
				BigDecimal lat = (BigDecimal) tt[4];
				BigDecimal lon = (BigDecimal) tt[5];
				result.add(new AccessPointPojo(apid.getId(), null, null, ssid,
						bssid, null, frequency, lat, lon));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response listWifi() {
		Session session = null;
		List<AccessPointPojo> result = new ArrayList<AccessPointPojo>();
		try {
			session = HibernateUtil.getSession();
			Criteria criteria = session.createCriteria(Ap.class);
			criteria.setMaxResults(150);
			for (Object a : criteria.list()) {
				ApId ap = ((Ap) a).getId();
				long id =  mac2Long(ap.getBssid());
				if(id == -1) {
					//ignore invalid ssid
					continue;
				}
				AccessPointPojo app = new AccessPointPojo(id, null,
						null, ap.getSsid(), ap.getBssid(), null, ap.getFrequency(),
						ap.getLatitude(), ap.getLongitude());
				result.add(app);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}
	
	@GET
	@Path("near")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listNearWifi(@QueryParam("latitude") double latitudeP,
			@QueryParam("longitude") double longitudeP,
			@QueryParam("distance") @DefaultValue("0.0005") double distanceP) {
		Session session = null;
		List<AccessPointPojo> list = new ArrayList<AccessPointPojo>();
		Map<String, Object> result = new HashMap<String, Object>();
		WifiSpectrum spectrum = new WifiSpectrum();
		result.put("wifi",list);
		BigDecimal latitude = BigDecimal.valueOf(latitudeP);
		BigDecimal longitude = BigDecimal.valueOf(longitudeP);
		Location queryLocation = new Location(latitude, longitude);
		BigDecimal distance = BigDecimal.valueOf(distanceP);
		try {
			session = HibernateUtil.getSession();
			Query criteria = session.createQuery("from Ap ap where (ap.id.latitude between :latf and :latt) and (ap.id.longitude between :lonf and :lont) and ap.id.frequency < 3000");
			criteria.setParameter("latf", latitude.subtract(distance));
			criteria.setParameter("latt", latitude.add(distance));
			criteria.setParameter("lonf", longitude.subtract(distance));
			criteria.setParameter("lont", longitude.add(distance));
			for (Object a : criteria.list()) {
				ApId ap = ((Ap) a).getId();
				long id =  mac2Long(ap.getBssid());
				if(id == -1) {
					//ignore invalid ssid
					continue;
				}
				AccessPointPojo app = new AccessPointPojo(id, null,
						null, ap.getSsid(), ap.getBssid(), ap.getLevel(), ap.getFrequency(),
						ap.getLatitude(), ap.getLongitude());
				list.add(app);
				double distanceFromWifi = queryLocation.distanceTo(app.getLocation());
				spectrum.put(ap.getFrequency(), 20.0 - WifiSpectrum.fspl(distanceFromWifi, ap.getFrequency()));
			}
			result.put("spectrum", new WifiSpectrumPojo(spectrum));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
		return Response.ok(result).build();
	}

	static private long mac2Long(String mac) {
	        String[] elements = mac.split(":");
	        if(elements.length != 6) {
	        	return -1;
	        }
	        long ret = 0;
	        byte[] addressInBytes = new byte[6];
	        for (int i = 0; i < 6; i++) {
	            String element = elements[i];
	            addressInBytes[i] = (byte)Integer.parseInt(element, 16);
	        }
	        for (int i = 0; i < 6; i++) {
	            long t = (addressInBytes[i] & 0xffL) << ((5 - i) * 8);
	            ret |= t;
	        }
	        return ret;
	}
}
