package gr.uoa.di.hibernate.entities;
// Generated 02-Jul-2015 03:40:57 by Hibernate Tools 3.2.2.GA


import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ap generated by hbm2java
 */
@Entity
@Table(name="ap"
    ,catalog="diaxeirisi"
)
public class Ap  implements java.io.Serializable {


     private ApId id;

    public Ap() {
    }

    public Ap(ApId id) {
       this.id = id;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="count", column=@Column(name="count", nullable=false) ), 
        @AttributeOverride(name="ssid", column=@Column(name="ssid") ), 
        @AttributeOverride(name="bssid", column=@Column(name="bssid", length=17) ), 
        @AttributeOverride(name="frequency", column=@Column(name="frequency") ), 
        @AttributeOverride(name="level", column=@Column(name="level") ), 
        @AttributeOverride(name="latitude", column=@Column(name="latitude", nullable=false, precision=16, scale=14) ), 
        @AttributeOverride(name="longitude", column=@Column(name="longitude", nullable=false, precision=16, scale=14) ) } )
    public ApId getId() {
        return this.id;
    }
    
    public void setId(ApId id) {
        this.id = id;
    }




}


