package gr.uoa.di.hibernate.entities;
// Generated 24-Jun-2015 23:08:14 by Hibernate Tools 3.2.2.GA


import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * Gps generated by hbm2java
 */
@Entity
@Table(name="gps"
    ,catalog="diaxeirisi"
)
public class Gps  implements java.io.Serializable {


     private GpsId id;
     private Date timestamp;
     private Users users;
     private BigDecimal latitude;
     private BigDecimal longitude;

    public Gps() {
    }

	
    public Gps(GpsId id, Users users) {
        this.id = id;
        this.users = users;
    }
    public Gps(GpsId id, Users users, BigDecimal latitude, BigDecimal longitude) {
       this.id = id;
       this.users = users;
       this.latitude = latitude;
       this.longitude = longitude;
    }
   
     @EmbeddedId
    
    @AttributeOverrides( {
        @AttributeOverride(name="idgps", column=@Column(name="idgps", nullable=false) ), 
        @AttributeOverride(name="user", column=@Column(name="user", nullable=false, length=45) ) } )
    public GpsId getId() {
        return this.id;
    }
    
    public void setId(GpsId id) {
        this.id = id;
    }
    @Version@Temporal(TemporalType.TIMESTAMP)
    @Column(name="timestamp", length=19)
    public Date getTimestamp() {
        return this.timestamp;
    }
    
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="user", nullable=false, insertable=false, updatable=false)
    public Users getUsers() {
        return this.users;
    }
    
    public void setUsers(Users users) {
        this.users = users;
    }
    
    @Column(name="latitude", precision=17, scale=14)
    public BigDecimal getLatitude() {
        return this.latitude;
    }
    
    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }
    
    @Column(name="longitude", precision=17, scale=14)
    public BigDecimal getLongitude() {
        return this.longitude;
    }
    
    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }




}


