package gr.uoa.di.hibernate;

import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.function.StandardSQLFunction;

public class CustomDialect extends MySQL5Dialect {

	public CustomDialect() {
		super();
		registerFunction("geomidpoint", new StandardSQLFunction("geomidpoint"));
	}

}
