package gr.uoa.di.pojo;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({ "latitude", "longitude" })
public class BaseStationPojo {

	private String country;
	private String operator;
	private Integer cid;
	private Integer lac;
	private Location location;
	private Date date;
	private BigDecimal latitude;
	private BigDecimal longitude;


	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BaseStationPojo() {
		// TODO Auto-generated constructor stub
	}

	public BaseStationPojo(long id, String country, String operator,
			Integer cid, Integer lac, BigDecimal latitude, BigDecimal longitude) {
		super();
		this.country = country;
		this.operator = operator;
		this.cid = cid;
		this.lac = lac;
		this.location = new Location(latitude, longitude);
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Integer getCid() {
		return cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

	public Integer getLac() {
		return lac;
	}

	public void setLac(Integer lac) {
		this.lac = lac;
	}

	public Location getLocation() {
		if(this.location == null) {
			return new Location(this.latitude,this.longitude);
		} else {
			return location;
		}
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
