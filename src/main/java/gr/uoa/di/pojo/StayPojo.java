package gr.uoa.di.pojo;

import java.math.BigDecimal;
import java.util.Comparator;

import org.apache.commons.math3.ml.clustering.Clusterable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties({ "POLAR_ORDER" ,"point" })
public class StayPojo implements Clusterable , Comparable<StayPojo>{

	public final Comparator<StayPojo> POLAR_ORDER = new PolarOrder();
	
	private Location location;
	private double tStart;
	private double tEnd;

	public StayPojo() {
	}

	public StayPojo(BigDecimal latitude, BigDecimal longitude, double tStart,
			double tEnd) {
		super();
		this.location = new Location(latitude, longitude);
		this.tStart = tStart;
		this.tEnd = tEnd;
	}

	public StayPojo(Location loc, double tStart, double tEnd) {
		super();
		this.location = loc;
		this.tStart = tStart;
		this.tEnd = tEnd;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public double gettStart() {
		return tStart;
	}

	public void setStart(double tStart) {
		this.tStart = tStart;
	}

	public double gettEnd() {
		return tEnd;
	}

	public void setEnd(double tEnd) {
		this.tEnd = tEnd;
	}

	@Override
	public double[] getPoint() {
		double[] ret = new double[2];
		ret[0] = this.location.getX().doubleValue();
		ret[1] = this.location.getY().doubleValue();
		return ret;
	}
	
	 /**
     * Is a->b->c a counterclockwise turn?
     * @param a first point
     * @param b second point
     * @param c third point
     * @return { -1, 0, +1 } if a->b->c is a { clockwise, collinear; counterclocwise } turn.
     */
	public static int ccw(StayPojo a, StayPojo b, StayPojo c) {
    	//(b.x-a.x)*(c.y-a.y) - (b.y-a.y)*(c.x-a.x);
    	BigDecimal op1 = b.location.getX().subtract(a.location.getX());
    	BigDecimal op2 = c.location.getY().subtract(a.location.getY());
    	BigDecimal op3 = b.location.getY().subtract(a.location.getY());
    	BigDecimal op4 = c.location.getX().subtract(a.location.getX());
    	BigDecimal sub = op1.multiply(op2).subtract(op3.multiply(op4));
        double area2 = sub.doubleValue();
        if      (area2 < 0) return -1;
        else if (area2 > 0) return +1;
        else                return  0;
    }
	
	// compare other points relative to polar angle (between 0 and 2pi) they
	// make with this Point
	private class PolarOrder implements Comparator<StayPojo> {
		public int compare(StayPojo q1, StayPojo q2) {
			double dx1 = q1.location.getX().subtract(location.getX()).doubleValue(); // q1.x - x;
			double dy1 = q1.location.getY().subtract(location.getY()).doubleValue(); // q1.y - y;
			double dx2 = q2.location.getX().subtract(location.getX()).doubleValue(); // q2.x - x;
			double dy2 = q2.location.getY().subtract(location.getY()).doubleValue(); // q2.y - y;

			if (dy1 >= 0 && dy2 < 0)
				return -1; // q1 above; q2 below
			else if (dy2 >= 0 && dy1 < 0)
				return +1; // q1 below; q2 above
			else if (dy1 == 0 && dy2 == 0) { // 3-collinear and horizontal
				if (dx1 >= 0 && dx2 < 0)
					return -1;
				else if (dx2 >= 0 && dx1 < 0)
					return +1;
				else
					return 0;
			} else
				return -ccw(StayPojo.this, q1, q2); // both above or below
		}
	}

	@Override
	public int compareTo(StayPojo that) {
        if (this.location.getY().compareTo(that.location.getY()) < 0) return -1;
        if (this.location.getY().compareTo(that.location.getY()) > 0) return +1;
        if (this.location.getX().compareTo(that.location.getX()) < 0) return -1;
        if (this.location.getX().compareTo(that.location.getX()) > 0) return +1;
        return 0;
	}
}
