package gr.uoa.di.pojo;

import java.math.BigDecimal;
import java.util.Date;

public class StampPojo {

	private Location location;
	private Date timestamp;

	public StampPojo() {
	}

	public StampPojo(java.util.Date date, BigDecimal latitude,
			BigDecimal longitude) {
		super();
		this.location = new Location(latitude, longitude);
		this.timestamp = date;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Date getDate() {
		return timestamp;
	}

	public void setDate(Date timestamp) {
		this.timestamp = timestamp;
	}

}
