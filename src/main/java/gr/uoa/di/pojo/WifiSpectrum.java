package gr.uoa.di.pojo;

import java.util.LinkedHashMap;

/**
 * Wifi Spectrum pojo only for 802.11 @2.4Ghz
 * 
 */
public class WifiSpectrum extends LinkedHashMap<Integer, Double> {

	private static final long serialVersionUID = 7569617755128686778L;
	public final static int NUMBER_OF_CHANNELS = 13;
	public final static int START_FREQUENCY = 2412;
	public final static int END_FREQUENCY = 2472;
	public final static int CHANNEL_BANDWITH = 5;

	public WifiSpectrum() {
		super();
		// populate with -Inf dB each channel
		for (int i = 0; i < NUMBER_OF_CHANNELS; i++) {
			super.put(new Integer(START_FREQUENCY + i * CHANNEL_BANDWITH),
					Double.NEGATIVE_INFINITY);
		}
	}

	/**
	 * 
	 * @param distance
	 *            from the access point
	 * @param frequency
	 *            of the access point
	 * @return the free space path loss in decibel
	 */
	static public double fspl(double distance, int frequency) {
		final double con = -27.55;
		return 20 * Math.log10(distance) + 20 * Math.log10(frequency) + con;
	}

	/**
	 * converts dBm to mWatt
	 * 
	 * @param db
	 * @return mWatt
	 */
	static public double dbTomWatt(double db) {
		return Math.pow(10.0, db / 10.0);
	}

	/**
	 * converts mWatt to dBm
	 * 
	 * @param mWatt
	 * @return dBm
	 */
	static public double mWattToDb(double mWatt) {
		return Math.log10(mWatt) * 10;
	}

	@Override
	public Double put(Integer key, Double value) {
		double valuemWatt = dbTomWatt(value);
		// silent reject
		if (!(key <= END_FREQUENCY && key >= START_FREQUENCY)) {
			return null;
		} else {
			// for +- 2 from base point update interferance
			for (int i = -2; i < 5; i++) {
				// System.out.println(key + (CHANNEL_BANDWITH * i));
				if (!this
						.containsKey(new Integer(key + (CHANNEL_BANDWITH * i)))) {
					continue;
				}
				Double prevValuemWatt = dbTomWatt(this.get(new Integer(key
						+ (CHANNEL_BANDWITH * i))));
				super.put(
						new Integer(key + (CHANNEL_BANDWITH * i)),
						mWattToDb(prevValuemWatt + valuemWatt
								* (1 / Math.pow(10, Math.abs(i)))));
			}
		}
		return value;
	}

}
