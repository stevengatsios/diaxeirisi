package gr.uoa.di.pojo;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Location implements Comparable<Location> {

	/**
     * Compares two points by polar angle (between 0 and 2pi) with respect to this point.
     */
	
	private BigDecimal x;
	private BigDecimal y;

	public Location(BigDecimal x, BigDecimal y) {
		this.setX(x);
		this.setY(y);
	}
	
	public Location() {
		// empty
	}

	public BigDecimal getY() {
		return y;
	}

	public void setY(BigDecimal y) {
		this.y = y;
	}

	public BigDecimal getX() {
		return x;
	}

	public void setX(BigDecimal x) {
		this.x = x;
	}

	@Override
	public int compareTo(Location that) {
        if (this.y.compareTo(that.y) < 0) return -1;
        if (this.y.compareTo(that.y) > 0) return +1;
        if (this.x.compareTo(that.x) < 0) return -1;
        if (this.x.compareTo(that.x) > 0) return +1;
        return 0;
	}
	
    /**
     * Return a string representation of this point.
     * @return a string representation of this point in the format (x, y)
     */
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    /**
     * Returns an integer hash code for this point.
     * @return an integer hash code for this point
     */
    public int hashCode() {
        int hashX = this.x.hashCode();
        int hashY = this.y.hashCode();
        return 31*hashX + hashY;
    }
	
	/**
	 * Converts a BigDecimal to radians
	 * point_radian = point_degrees * (pi / 180)
	 * @param point
	 * @return
	 */
	public static BigDecimal toRadian(BigDecimal point) {
		return point.multiply(new BigDecimal(Math.PI / 180.0));
	}
	
	/**
     * Returns the Haversine distance between this point and that point.
     * @param that the other point
     * @return the Haversine distance between this point and that point
     */
    public double distanceTo(Location that) {
    	double R = 6371000;
    	double f1 = toRadian(this.x).doubleValue();
    	double f2 = toRadian(that.x).doubleValue();
    	double Df = toRadian(that.x.subtract(this.x)).doubleValue();
    	double Dl = toRadian(that.y.subtract(this.y)).doubleValue();
    	double a = Math.sin(Df/2) * Math.sin(Df/2) + Math.cos(f1)*Math.cos(f2)*(Math.sin(Dl/2) * Math.sin(Dl/2));
    	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    	return R * c;
    }

}
