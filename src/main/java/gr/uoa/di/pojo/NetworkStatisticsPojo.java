package gr.uoa.di.pojo;

public class NetworkStatisticsPojo {

	private long users;
	private String network;
	private String country;
	private String iso;

	public NetworkStatisticsPojo(long users, String network, String country,
			String iso) {
		super();
		this.users = users;
		this.network = network;
		this.country = country;
		this.iso = iso;
	}

	public NetworkStatisticsPojo() {
		// TODO Auto-generated constructor stub
	}

	public long getUsers() {
		return users;
	}

	public void setUsers(long users) {
		this.users = users;
	}

	public String getNetwork() {
		return network;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getIso() {
		return iso;
	}

	public void setIso(String iso) {
		this.iso = iso;
	}

}
