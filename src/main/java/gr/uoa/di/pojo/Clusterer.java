package gr.uoa.di.pojo;

import org.apache.commons.math3.ml.clustering.DBSCANClusterer;

public class Clusterer extends DBSCANClusterer<StayPojo>{

	final public static HaversineDistance distance = new HaversineDistance();
	
	public Clusterer(double eps, int minPts) {
		super(eps, minPts,distance);
	}
}
