package gr.uoa.di.pojo;

import java.util.Date;

public class BatteryPojo {

    private Date timestamp;
    private Integer level;
    private Byte plugged;
    private Integer temperature;
    private Integer voltage;
	
	public BatteryPojo() {
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Byte getPlugged() {
		return plugged;
	}

	public void setPlugged(Byte plugged) {
		this.plugged = plugged;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public Integer getVoltage() {
		return voltage;
	}

	public void setVoltage(Integer voltage) {
		this.voltage = voltage;
	}

	
}
