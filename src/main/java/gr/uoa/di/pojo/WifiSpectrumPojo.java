package gr.uoa.di.pojo;

import gr.uoa.di.pojo.WifiSpectrumPojo.WifiSpectrumItemPojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;

public class WifiSpectrumPojo extends ArrayList<WifiSpectrumItemPojo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8710218410173944742L;
	public static final double MAXIMUM_DB = -30.0;
	public static final double MINIMUM_DB = -100.0;
	public static final double GAMMA = 2;
	public WifiSpectrumPojo() {
		// TODO Auto-generated constructor stub
	}
	
	public WifiSpectrumPojo(WifiSpectrum spectrum) {
//		Double max = Collections.max(spectrum.values());
//		Double min = Collections.min(spectrum.values());
		for(Entry<Integer,Double> entry : spectrum.entrySet()) {
			WifiSpectrumItemPojo item;
			
			Double normalizedValue = 100.0 * (1 - Math.pow((entry.getValue() - MINIMUM_DB)/(MAXIMUM_DB - MINIMUM_DB),GAMMA));
			if(entry.getValue() < MINIMUM_DB) {
				item = new WifiSpectrumItemPojo(entry.getKey(), 100.0);
			} else if (normalizedValue < 0){
				item = new WifiSpectrumItemPojo(entry.getKey(), 0.0);
			} else {
				item = new WifiSpectrumItemPojo(entry.getKey(), normalizedValue);
			}
			this.add(item);
		}
	}

	public class WifiSpectrumItemPojo {
		private Integer frequency;
		private Double level;

		public WifiSpectrumItemPojo() {

		}

		public WifiSpectrumItemPojo(Integer frequency, Double level) {
			super();
			this.frequency = frequency;
			this.level = level;
		}

		public Integer getFrequency() {
			return frequency;
		}

		public void setFrequency(Integer frequency) {
			this.frequency = frequency;
		}

		public Double getLevel() {
			return level;
		}

		public void setLevel(Double level) {
			this.level = level;
		}

	}

}
