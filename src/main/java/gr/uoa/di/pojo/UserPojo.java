package gr.uoa.di.pojo;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserPojo {

	private String user;
	private Location location;
	private Integer probability;
	private Integer contribution;
	private Timestamp minDate;
	private Timestamp maxDate;
	private StampListPojo gps;
	private List<AccessPointPojo> wifi;
	private List<BaseStationPojo> bases;
	private List<BatteryPojo> battery;

	public UserPojo(String user, BigDecimal latitude, BigDecimal longitude,
			Integer probability,Integer contribution) {
		super();
		this.user = user;
		this.location = new Location(latitude, longitude);
		this.probability = probability;
		this.contribution = contribution;
	}

	public UserPojo() {
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Integer getProbability() {
		return probability;
	}

	public void setProbability(Integer probability) {
		this.probability = probability;
	}

	public Integer getContribution() {
		return contribution;
	}

	public void setContribution(Integer contribution) {
		this.contribution = contribution;
	}

	public Timestamp getMinDate() {
		return minDate;
	}

	public void setMinDate(Timestamp minDate) {
		this.minDate = minDate;
	}

	public Timestamp getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(Timestamp maxDate) {
		this.maxDate = maxDate;
	}

	public StampListPojo getGps() {
		return gps;
	}

	public void setGps(StampListPojo gps) {
		this.gps = gps;
	}

	public List<AccessPointPojo> getWifi() {
		return wifi;
	}

	public void setWifi(List<AccessPointPojo> wifi) {
		this.wifi = wifi;
	}

	public List<BaseStationPojo> getBases() {
		return bases;
	}

	public void setBases(List<BaseStationPojo> bases) {
		this.bases = bases;
	}

	public List<BatteryPojo> getBattery() {
		return battery;
	}

	public void setBattery(List<BatteryPojo> battery) {
		this.battery = battery;
	}
	
	

}
