package gr.uoa.di.pojo;

import gr.uoa.di.hibernate.entities.Gps;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class StayPointsList {
	private double tmin;
	private double tmax;
	private double dmax;

	public StayPointsList(double tmin, double tmax, double dmax) {
		this.tmin = tmin;
		this.tmax = tmax;
		this.dmax = dmax;
	}

	public double getTmin() {
		return tmin;
	}

	public void setTmin(double tmin) {
		this.tmin = tmin;
	}

	public double getTmax() {
		return tmax;
	}

	public void setTmax(double tmax) {
		this.tmax = tmax;
	}

	public double getDmax() {
		return dmax;
	}

	public void setDmax(double dmax) {
		this.dmax = dmax;
	}

	public List<StayPojo> getStayPointsList(List<Gps> gps) {
		int i = 0;
		int N = gps.size();
		//System.out.printf("dmax %f tmin %f tmax %f\n", dmax, tmin, tmax);
		List<StayPojo> stayPList = new ArrayList<StayPojo>();
		while (i < N - 1) {
			int j = i + 1;
			//System.out.println("outer loop i = [" + i + "]");
			while (j < N) {
				//System.out.println("inner loop j = [" + j + "]");
				long tj = gps.get(j).getTimestamp().getTime();
				long ti = gps.get(i).getTimestamp().getTime();
				long tj_1 = gps.get(j - 1).getTimestamp().getTime();
				double dtime1 = Math.abs(tj - tj_1);
				double dtime2 = Math.abs(ti - tj_1);
				double dtime3 = Math.abs(ti - tj);
				Location li = new Location(gps.get(i).getLatitude(), gps.get(i)
						.getLongitude());
				Location lj = new Location(gps.get(j).getLatitude(), gps.get(j)
						.getLongitude());
				double dist = li.distanceTo(lj);
//				System.out.printf(
//						"distance %f dtime1 %f dtime2 %f dtime3 %f\n", dist,
//						dtime1, dtime2, dtime3);
				if (dtime1 > tmax) {
					if (dtime3 > tmin) {
						List<Gps> gpsSublist = getSubList(gps, i, j - 1);
						Location coords = estimateCendroid(gpsSublist);
						long timeStart = gpsSublist.get(0).getTimestamp()
								.getTime();
						long timeEnd = gpsSublist.get(gpsSublist.size() - 1)
								.getTimestamp().getTime();
						//System.out.printf("brika enaaaa1");
						stayPList.add(new StayPojo(coords, timeStart, timeEnd));
					}
					i = j;
					break;
				} else if (dist > dmax) {
					if (dtime2 > tmin) {
						List<Gps> gpsSublist = getSubList(gps, i, j - 1);
						Location coords = estimateCendroid(gpsSublist);
						long timeStart = gpsSublist.get(0).getTimestamp()
								.getTime();
						long timeEnd = gpsSublist.get(gpsSublist.size() - 1)
								.getTimestamp().getTime();
						//System.out.printf("brika enaaaa2");
						stayPList.add(new StayPojo(coords, timeStart, timeEnd));
						i = j;
						break;
					}
					i++;
					break;
				} else if (j == N - 1) {
					if (dtime3 > tmin) {
						List<Gps> gpsSublist = getSubList(gps, i, j);
						Location coords = estimateCendroid(gpsSublist);
						long timeStart = gpsSublist.get(0).getTimestamp()
								.getTime();
						long timeEnd = gpsSublist.get(gpsSublist.size() - 1)
								.getTimestamp().getTime();
						//System.out.printf("brika enaaaa3");
						stayPList.add(new StayPojo(coords, timeStart, timeEnd));
					}
					i = j;
					break;
				}
				j++;
			}

		}
		return stayPList;
	}

	private static Location estimateCendroid(List<Gps> list) {
		// BigDecimal[] coords = new BigDecimal[2];
		Location coords = new Location(BigDecimal.valueOf(0),
				BigDecimal.valueOf(0));
		for (Gps gps : list) {
			coords.setX(coords.getX().add(gps.getLatitude()));
			coords.setY(coords.getY().add(gps.getLongitude()));
		}
		coords.setX(coords.getX().divide(new BigDecimal(list.size()), 12,
				RoundingMode.HALF_UP));
		coords.setY(coords.getY().divide(new BigDecimal(list.size()), 12,
				RoundingMode.HALF_UP));
		return coords;
	}
	
	private List<Gps> getSubList(List<Gps> list, int begin, int end){
		List<Gps> subList = new ArrayList<Gps>();
		if (begin == end)
			subList.add(0, list.get(begin));
		else
			subList = list.subList(begin, end);
		return subList;
	}

}
