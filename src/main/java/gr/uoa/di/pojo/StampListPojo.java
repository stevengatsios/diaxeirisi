package gr.uoa.di.pojo;

import java.util.Date;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "current", "base" })
public class StampListPojo extends ArrayList<StampPojo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1859411201670479423L;
	private Date quantization;
	private Date current;

	// private Integer base;

	public StampListPojo() {
		super();
	}

	public StampListPojo(Date quantization) {
		super();
		this.current = new Date(0);
		// this.base = -1;
		this.quantization = quantization;
	}

	public Date getQuantization() {
		return quantization;
	}

	public void setQuantization(Date quantization) {
		this.quantization = quantization;
	}

	@Override
	public boolean add(StampPojo e) {
		if (e == null) {
			throw new NullPointerException();
		}
		// is going to be added when time diff within quantization
		// or base station has chaneged
		boolean in = Math.abs(e.getDate().getTime() - current.getTime()) >= quantization
				.getTime();
		// in = in || (base != e.getBaseCid());
		if (in) {
			current.setTime(e.getDate().getTime());
			// base = e.getBaseCid();
			return super.add(e);
		} else {
			return false;
		}
	}
}
