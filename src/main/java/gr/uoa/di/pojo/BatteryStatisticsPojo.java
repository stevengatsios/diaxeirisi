package gr.uoa.di.pojo;

public class BatteryStatisticsPojo {

	private int hour;
	private long count;
	private double avg;

	public BatteryStatisticsPojo(int level, long count, double avg) {
		super();
		this.hour = level;
		this.count = count;
		this.avg = avg;
	}

	public BatteryStatisticsPojo() {
		// TODO Auto-generated constructor stub
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int level) {
		this.hour = level;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public double getAvg() {
		return avg;
	}

	public void setAvg(double avg) {
		this.avg = avg;
	}

}
