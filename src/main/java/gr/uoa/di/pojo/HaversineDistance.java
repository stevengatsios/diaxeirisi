package gr.uoa.di.pojo;

import org.apache.commons.math3.ml.distance.DistanceMeasure;

public class HaversineDistance implements DistanceMeasure {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7351190425049529352L;

	public HaversineDistance() {
		// TODO Auto-generated constructor stub
	}


	@Override
	public double compute(double[] arg0, double[] arg1) {
		double R = 6371000;
    	double f1 = toRadian(arg0[0]);
    	double f2 = toRadian(arg1[0]);
    	double Df = toRadian(arg1[0] - (arg0[0]));
    	double Dl = toRadian(arg1[1] - (arg0[1]));
    	double a = Math.sin(Df/2) * Math.sin(Df/2) + Math.cos(f1)*Math.cos(f2)*(Math.sin(Dl/2) * Math.sin(Dl/2));
    	double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    	return R * c;
	}

	public static double toRadian(double d) {
		return d * (Math.PI / 180.0);
	}
	

}
