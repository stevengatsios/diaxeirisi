package gr.uoa.di.pojo;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties({ "latitude", "longitude" })
public class AccessPointPojo {

	private long id;
    private Date timestamp;
    private String user;
    private String ssid;
    private String bssid;
    private Double level;
    private Integer frequency;
    private Location location;
	private BigDecimal latitude;
	private BigDecimal longitude;

	public AccessPointPojo() {
		// TODO Auto-generated constructor stub
	}

	public AccessPointPojo(long id, Date timestamp, String user,
			String ssid, String bssid, Double level, Integer frequency,
			BigDecimal latitude, BigDecimal longitude) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.user = user;
		this.ssid = ssid;
		this.bssid = bssid;
		this.level = level;
		this.frequency = frequency;
		this.location = new Location(latitude,longitude);
	}

	public String getBssid() {
		return bssid;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public long getId() {
		return id;
	}
	
	public BigDecimal getLatitude() {
		return latitude;
	}

	public Double getLevel() {
		return level;
	}

	public Location getLocation() {
		if(this.location == null) {
			return new Location(this.latitude,this.longitude);
		} else {
			return location;
		}
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public String getSsid() {
		return ssid;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public String getUser() {
		return user;
	}

	public void setBssid(String bssid) {
		this.bssid = bssid;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public void setLevel(Double level) {
		this.level = level;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public void setUser(String user) {
		this.user = user;
	}
	
	

}
