--find contribution of users
 SELECT g.user,count(*) as count from gps as g group by g.user;
--update contribution of users batch
UPDATE users SET contribution = (SELECT g.user,count(*) as count from gps as g where g.user=users.user group by g.user);

--get statistics for network
SELECT count(distinct user) as count,network,country from base_stations b, country c WHERE b.mcc = c.mcc and b.mnc = c.mnc GROUP BY country,network limit 100;

--create view
select count(*) AS `count`,`access_points`.`ssid` AS `ssid`,`access_points`.`bssid` AS `bssid`,`access_points`.`frequency` AS `frequency`, `access_points`.`level` as `level`,geomidpoint(`access_points`.`latitude`,`access_points`.`level`) AS `latitude`,geomidpoint(`access_points`.`longitude`,`access_points`.`level`) AS `longitude` from `access_points` group by `access_points`.`bssid` order by count(*)